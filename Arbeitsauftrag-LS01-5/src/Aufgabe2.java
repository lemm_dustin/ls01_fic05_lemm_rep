
public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.printf("Hello %s!%n", "World");  
		
		// linksbŁndig mit 20 Stellen
		
		String s = "Java-Programm";
		System.out.printf( "|%-20s|\n", s );    // |Java-Programm       |
		
		int n0 = 0;
		int n1 = 1;
		int n2 = 2;
		int n3 = 3;
		int n4 = 4;
		int n5 = 5;
				
		
		
		String s0 = "0!";
		String s1 = "1!";
		String s2 = "2!";
		String s3 = "3!";
		String s4 = "4!";
		String s5 = "5!";
		
		
		System.out.print(	"%-5n0	\n" s0
						+	"%-5s1	\n" s1
						+	"%-5s2	\n" s2
						+	"%-5s3	\n" s3
						+	"%-5s4	\n" s4
						+	"%-5s5	\n" s5);
		
		//erst als int und dann als ausrufezeiche nals strings?
	}

}

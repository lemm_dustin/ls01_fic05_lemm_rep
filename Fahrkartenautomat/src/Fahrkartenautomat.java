import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);
	 
    public static void main(String[] args)
    {
    	//einlesen Ticketpreis + Anzahl
    	while(true) {
    		int zuZahlenderBetrag = fahrkartenbestellungErfassen();
    		int r�ckgabewert = fahrkartenBezahlen(zuZahlenderBetrag);
    		fahrkartenAusgeben();
    		rueckgeldAusgeben(r�ckgabewert);
    		System.out.print("\n\n\n\n\n\n");
    	}
    	
    	//tastatur.close();
    }
    
    
    
    
    
    //Ticketpreis * Anzahl
    public static int fahrkartenbestellungErfassen() {
    	 
    	int zuZahlenderBetrag = 0;
    	int anzahlTickets = 0;
    	char befehl = '0';
    	

    	System.out.print("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n" + 
    				"  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n" + 
    				"  Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n" + 
    				"  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)" +
    				"  \nIhre Wahl: ");
    		
    	while(befehl != '1' && befehl != '2' && befehl != '3') {
    		befehl = tastatur.next().charAt(0);
    		if(befehl != '1' && befehl != '2' && befehl != '3') {
    			System.out.print("\nUng�ltige Eingabe\nIhre Wahl: ");
    		}
    	}
    		
    		switch (befehl) {
    			case '1':
    				zuZahlenderBetrag = 290;
    				break;
    			case '2':
    				zuZahlenderBetrag = 860;
    				break;
    			case '3':
    				zuZahlenderBetrag = 2350;
    				break;
    			default:
    				System.out.println("Ung�ltige Eingabe");
    		}
    	
    	while(anzahlTickets < 1 || anzahlTickets > 10) {
        	System.out.print("Anzahl dieser Ticketart: ");
        	anzahlTickets = tastatur.nextInt();
        	if(anzahlTickets < 1 || anzahlTickets > 10) {
        		System.out.println("Bitte geben Sie einen Wert zwischen 1 und 10 ein.");
        	}
         }
         
         zuZahlenderBetrag = zuZahlenderBetrag * anzahlTickets;
         return zuZahlenderBetrag;
    	
    }
    
    public static int fahrkartenBezahlen(int zuZahlenderBetrag) {
    	int eingezahlterGesamtbetrag = 0;
    	int eingeworfeneM�nze;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f\n", (((double)(zuZahlenderBetrag - eingezahlterGesamtbetrag))/100));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = (int) (tastatur.nextDouble() * 100);
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void warte(int pause) {
    	try {
 			Thread.sleep(pause);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static void rueckgeldAusgeben( int r�ckgabebetrag) {
         if(r�ckgabebetrag > 0)
         {
      	 //FA-02: durch %.2f wird der double mit zwei Nachkommastellen ausgegeben
      	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", (((double)r�ckgabebetrag)/100));
      	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           muenzeAusgeben(r�ckgabebetrag);
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    public static void muenzeAusgeben(int r�ckgabebetrag){
    	while(r�ckgabebetrag >= 200) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 200;
        }
        while(r�ckgabebetrag >= 100) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 100;
        }
        while(r�ckgabebetrag >= 50) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 50;
        }
        while(r�ckgabebetrag >= 20) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          r�ckgabebetrag -= 20;
        }
        while(r�ckgabebetrag >= 10) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 10;
        }
        while(r�ckgabebetrag >= 5)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          r�ckgabebetrag -= 5;
        }
    }
    
}